<?php

return [
    'token' => env('D3_TOKEN', ""),
    'url' => env('D3_URL', ""),
];
