<?php

use App\Http\Controllers\DocumentController;
use App\Http\Controllers\ScanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['tenant']], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
    Route::get('/inboundExtensions', [App\Http\Controllers\HomeController::class, 'inboundExtensions'])->name("homefeatures");

    Route::get('scan/{scan}', [ScanController::class, 'show']);
    Route::post('scan/{scan}', [ScanController::class, 'download']);

    Route::get("document/{document}", [DocumentController::class, "show"]);

    Route::get("test", function() { return view("success")->with(["documents" => App\Models\Scan::find(1)->documents]); });
});