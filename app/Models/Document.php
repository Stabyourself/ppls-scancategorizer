<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    use HasFactory;

    protected $fillable = ["scan_id", "name", "success_callback_uri", "download_url"];

    public function scan()
    {
        return $this->belongsTo("App\Models\Scan");
    }

    public function download()
    {
        $downloadUrl = $this->download_url;

        Storage::makeDirectory($this->scan->id);

        $path = $this->getTempFilename();

        $client = new Client();

        $resource = fopen(Storage::path($path), 'w');

        $response = $client->request('GET', $downloadUrl, [
            'headers' => [
                'Authorization' => 'Bearer ' . config("d3.token"),
                'Content-Type' => 'application/pdf'
            ],
            'sink' => $resource,
        ]);

        $this->path = $path;
        $this->save();
    }

    public function getTempFilename()
    {
        return "{$this->scan->id}/{$this->id}.pdf";
    }

    public function successCallback()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . config("d3.token"),
        ])->post($this->success_callback_uri, [
            "userData" => [
                [
                    "key" => "deleteBatchUserData",
                    "values" => [
                        "delete",
                    ]
                ]
            ]
        ]);

        return $response;
    }
}
