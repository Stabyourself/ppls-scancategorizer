<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!empty($request->header("x-forwarded-host"))) {
            URL::forceRootUrl("https://" . $request->header("x-forwarded-host") . "/ppls-scancategorizer");
            URL::forceScheme("https");
        }

        if (App::environment(["cloud"])) { // skip this middleware on local/prem
            $tenant = $request->header("x-dv-tenant-id");
            $baseUri = $request->header("x-dv-baseuri");
            $appSecretEncoded = config("app.dvelop_app_secret");
            $expectedSigEncoded = $request->header("x-dv-sig-1");

            $appSecret = base64_decode($appSecretEncoded);
            $mySigEncoded = base64_encode(hash_hmac("sha256", $baseUri . $tenant, $appSecret, true));

            if ($mySigEncoded != $expectedSigEncoded) {
                abort(403);
            }
        }

        return $next($request);
    }
}
