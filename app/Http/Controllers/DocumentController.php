<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    public function show(Document $document)
    {
        if (!Storage::exists($document->getTempFilename())) {
            $document->download();
        }

        return response()->file(Storage::path($document->path));
    }
}
