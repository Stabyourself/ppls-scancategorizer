<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Scan;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ScanController extends Controller
{
    public function store(Request $request)
    {
        $content = $request->getContent();

        $documents = json_decode($content)->documents;

        // make scan
        $scan = Scan::create();

        foreach ($documents as $document) {
            $downloadUrl = "";

            foreach ($document->exportFiles as $exportFile) {
                if ($exportFile->extension == ".pdf") {
                    $downloadUrl = $exportFile->downloadUrl;
                }
            }

            Document::create([
                "scan_id" => $scan->id,
                "name" => null,
                "success_callback_uri" => config("d3.url") . $document->successCallbackUri,
                "download_url" => config("d3.url") . $downloadUrl,
            ]);
        }

        $url = "/ppls-scancategorizer/scan/{$scan->id}";
        return response("", 201)->header("Location", $url);
    }

    public function show(Scan $scan)
    {
        $documents = $scan->documents()->get(["name", "id", "download_url"]);

        foreach ($documents as $document) {
            $document->category = null;
            $document->subCategory = null;
        }

        return view("show")->with(compact("documents"));
    }

    public function download(Scan $scan, Request $request)
    {
        $validated = $request->validate([
            "category" => "array",
            "sub-category" => "array",
            "additional" => "array",
        ]);

        foreach ($scan->documents as $i=>$document) {
            $sourceFile = $document->getTempFilename();

            if (!Storage::exists($sourceFile)) {
                $document->download();
            }

            $fileNameParts = [];
            $fileNameParts[] = $validated['category'][$i];
            $fileNameParts[] = $validated['sub-category'][$i];

            if (!is_null($validated["additional"][$i])) {
                $fileNameParts[] = $validated["additional"][$i];
            }

            $fileNameParts[] = $document->id;

            $fileName = implode(" - ", $fileNameParts) . ".pdf";

            // strip bad characters
            $bad = array_merge(
                array_map('chr', range(0,31)),
                array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
            $fileName = str_replace($bad, "", $fileName);

            $destinationFile = "{$scan->id}/{$fileName}";


            $full_path_source = Storage::disk("temp")->path($sourceFile);
            $full_path_dest = Storage::disk("export")->path($destinationFile);

            // make folder
            if (!Storage::disk("export")->exists($scan->id)) {
                Storage::disk("export")->makeDirectory($scan->id);
            }

            File::move($full_path_source, $full_path_dest);

            $document->path = $destinationFile;
            $document->save();

            $document->successCallback();
        }

        return view("success")->with(["documents" => $scan->documents]);
    }
}
