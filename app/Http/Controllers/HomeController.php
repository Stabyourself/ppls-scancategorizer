<?php

namespace App\Http\Controllers;

use App\Models\Tile;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $info = [
            "_links" => [
                "inboundExtensions" => [
                    "href" => "/ppls-scancategorizer/inboundExtensions",
                ],
            ]
        ];

        return response()->json($info);
    }

    public function inboundExtensions()
    {
        $info = [
            "extensions" => [
                [
                    "id" => "ppls-scancategorizer-scan",
                    "caption" => "PPLS Scan Klassifizierung",
                    "context" => "ExportImportProcess",
                    "uriTemplate" => "/ppls-scancategorizer/scan"
                ]
            ]
        ];

        return response()->json($info);
    }
}
