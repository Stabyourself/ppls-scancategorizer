<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategorySeeder extends Seeder
{
    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFileName = "import.csv";
        $csvFile = storage_path($csvFileName);
        $categories = $this->readCSV($csvFile,array('delimiter' => ','));

        array_shift($categories); // remove header

        // truncate tables for existing data
        DB::table("sub_categories")->truncate();
        DB::table("categories")->truncate();

        foreach ($categories as $category) {
            if ($category) {
                $number = utf8_encode($category[1]);
                $name = $number . " - " . utf8_encode($category[2]);

                dump($name);

                $cat = Category::create([
                    "name" => $name,
                ]);

                $subCategories = explode("\n", $category[12]);

                $subCategories[] = "Clearing"; // client wish

                foreach ($subCategories as $subCategory) {
                    $name = trim(utf8_encode($subCategory));

                    if (!empty($name)) {
                        SubCategory::create([
                            "category_id" => $cat->id,
                            "name" => $name,
                        ]);
                    }
                }
            }
        }
    }
}
