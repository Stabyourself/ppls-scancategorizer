<?php

namespace Database\Seeders;

use App\Models\Document;
use App\Models\Scan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([CategorySeeder::class]);

        if (App::environment('local')) {
            Scan::create();

            Document::create([
                "download_url" => "https://upload.guegan.de/example.pdf",
                "scan_id" => 1,
                "success_callback_uri" => "localhost",
            ]);

            Document::create([
                "download_url" => "https://upload.guegan.de/example.pdf",
                "scan_id" => 1,
                "success_callback_uri" => "localhost",
            ]);

            Document::create([
                "download_url" => "https://upload.guegan.de/example.pdf",
                "scan_id" => 1,
                "success_callback_uri" => "localhost",
            ]);
        }
    }
}
