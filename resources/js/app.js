require('./bootstrap')
import Vue from 'vue'
import Vuetify from 'vuetify'

// Components
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue stuff
window.Vue = Vue
window.Vuetify = Vuetify

Vue.use(Vuetify)
