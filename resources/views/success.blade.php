@extends("layout.app")

@section("content")
    <h1>Import Erfolgreich :)</h1>

    <p>Folgende Dokumente wurden importiert:</p>

    @foreach ($documents as $document)
        <div><v-icon>mdi-file-pdf-outline</v-icon>{{ $document->path }}</div>
    @endforeach
@endsection