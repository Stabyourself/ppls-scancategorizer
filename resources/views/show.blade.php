@extends("layout.app")

@section("content")
    <scan-form
        api-url="{{ config('app.url_folder') }}"
        :categories='{!! json_encode(\App\Models\Category::with("subCategories")->get()) !!}'
        :document-data='{!! json_encode($documents) !!}'
    />
@endsection