<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>PPLS Cloud Apps</title>

        <link href="{{ asset(mix("css/app.css")) }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <v-app>
                <v-main>
                    <v-container fluid>
                        @yield('content')
                    </v-container>
                </v-main>
            </v-app>
        </div>

        <script src="{{ asset(mix('js/app.js')) }}"></script>
        <script>
            const app = new Vue({
                vuetify: new Vuetify()
            }).$mount('#app');
        </script>
    </body>
</html>
